# RedEfec #

## Description ##
Examen para RedEfectiva.

## Requirements ##
* [.NET Framework](http://www.microsoft.com/es-mx/download/details.aspx?id=30653)
* [Compilado](http://cdn.3utilities.com/RedEfec.zip)

## Developer Documentation ##
* [Doc](http://yorch.unixssh.com/re_doc/)
 
## Installation ##
Descargue http://cdn.3utilities.com/RedEfec.zip y descomprima.

Ejecute desde consola RedEfec.exe y el programa le pedirá el directorio a scannear, tambien puede
pasar el directorio a escanear como parametro.

Enseguida copie en archivo file1.txt al directorio especificado y éste cambiará de extensión
además de que se generará conciliacion.txt.

## Notes ##
Esta aplicación se desarrolló en Xubuntu con MonoDevelop.
Testeado en Xubuntu y Windows 7.

P.D. Let's go play !!!





