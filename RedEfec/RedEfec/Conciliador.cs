﻿using System;
using System.IO;
using System.Collections;
using System.Text;

namespace RedEfec
{
	// Conciliador 
	//
	// Conciliador Generate Conciliacion file
	//
	// Copyright 2015 Jorge Alberto Ponce Turrubiates
	//
	// Licensed under the Apache License, Version 2.0 (the "License");
	// you may not use this file except in compliance with the License.
	// You may obtain a copy of the License at
	//
	//     http://www.apache.org/licenses/LICENSE-2.0
	//
	// Unless required by applicable law or agreed to in writing, software
	// distributed under the License is distributed on an "AS IS" BASIS,
	// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	// See the License for the specific language governing permissions and
	// limitations under the License.
	public class Conciliador
	{
		/// <summary>
		/// ArrayList file.
		/// </summary>
		ArrayList aFile = new ArrayList();

		/// <summary>
		/// The default path.
		/// </summary>
		string dftpath = "";

		/// <summary>
		/// Constructor Class
		/// </summary>
		/// <param name="path">Path.</param>
		public Conciliador (string path)
		{
			dftpath = path;

			fileToArrayList ();
		}

		/// <summary>
		/// Files to array list.
		/// </summary>
		private void fileToArrayList()
		{
			if (File.Exists (dftpath + "conciliacion.txt")) 
			{
				StreamReader file =  new StreamReader(dftpath + "conciliacion.txt");
				string line = "";

				while((line = file.ReadLine()) != null)
				{
					if (!(line.StartsWith ("TOTAL"))) 
					{
						aFile.Add (new Record (line));
					}
				}

				file.Close ();
			}
		}

		/// <summary>
		/// Adds the file.
		/// </summary>
		/// <param name="content">Content.</param>
		public void addFileContent(string content)
		{
			string[] lines = content.Split ('\n');

			for (int i = 0; i < lines.Length; i++) 
			{
				aFile.Add (new Record (lines [i]));
			}
		}

		/// <summary>
		/// Saves the array to Conciliacion File.
		/// </summary>
		public void saveArray()
		{
			StreamWriter fileconc = new StreamWriter(dftpath + "conciliacion.txt");

			// Order by dticks
			aFile.Sort(new CustomComparer());

			decimal total = 0;

			foreach (Record lines in aFile)
			{
				if (lines.producto.Equals ("Compra"))
					total = total + decimal.Parse (lines.importe);
				
				fileconc.WriteLine (lines.oline);
			}

			if (total > 0)
				fileconc.WriteLine ("TOTAL: " + total.ToString());

			aFile = new ArrayList ();

			fileconc.Close ();
		}
	}

	/// <summary>
	/// Custom comparer for ArrayList.
	/// </summary>
	public class CustomComparer : IComparer
	{
		Comparer _comparer = new Comparer(System.Globalization.CultureInfo.CurrentCulture);

		/// <summary>
		/// Compare DateTime for Ticks
		/// </summary>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		public int Compare(object x, object y)
		{
			Record rx = (Record)x;
			Record ry = (Record)y;

			return _comparer.Compare(rx.dticks, ry.dticks);
		}
	}
}

