﻿using System;
using System.IO;
using System.Threading;

namespace RedEfec
{
	// DirScanner 
	//
	// DirScanner Scan directory
	//
	// Copyright 2015 Jorge Alberto Ponce Turrubiates
	//
	// Licensed under the Apache License, Version 2.0 (the "License");
	// you may not use this file except in compliance with the License.
	// You may obtain a copy of the License at
	//
	//     http://www.apache.org/licenses/LICENSE-2.0
	//
	// Unless required by applicable law or agreed to in writing, software
	// distributed under the License is distributed on an "AS IS" BASIS,
	// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	// See the License for the specific language governing permissions and
	// limitations under the License.
	public class DirScanner
	{
		/// <summary>
		/// The DELAY
		/// </summary>
		public const Int32 DELAY = 500;

		/// <summary>
		/// The scanpath.
		/// </summary>
		private string scanpath = "";

		/// <summary>
		/// The this lock.
		/// </summary>
		private Object thisLock = new Object();

		/// <summary>
		/// Initializes a new instance of the <see cref="RedEfec.DirScanner"/> class.
		/// </summary>
		/// <param name="path">Path.</param>
		public DirScanner (string path)
		{
			scanpath = path;
		}

		/// <summary>
		/// Start Scanning.
		/// </summary>
		public void start()
		{
			if (Directory.Exists (scanpath)) 
			{
				Console.WriteLine ("Press CTRL + C for Exit. Scanning " + scanpath);

				while (1 == 1)
					scan ();
			} 
			else 
			{
				Console.WriteLine ("The Directory does not Exists");
			}
		}

		/// <summary>
		/// Scan Directory.
		/// </summary>
		private void scan()
		{
			Thread.Sleep (DELAY);

			string[] txtfiles = Directory.GetFiles(scanpath, "*.txt");

			for (int i = 0; i < txtfiles.Length; i++) 
			{
				process (txtfiles[i]);
			}
		}

		/// <summary>
		/// Process the specified textfile.
		/// </summary>
		/// <param name="textfile">Textfile.</param>
		private void process(string textfile)
		{
			if (!(textfile.EndsWith ("conciliacion.txt"))) 
			{
				string content = File.ReadAllText (textfile);
				Console.WriteLine ("Processing " + textfile);
				Console.WriteLine (content);

				// Rename file
				long timestamp = DateTime.Now.Ticks;

				File.Move(textfile, textfile + "." + timestamp);

				conciliar (content);
			}
		}

		/// <summary>
		/// Generate Conciliacion File
		/// </summary>
		/// <param name="content">Content.</param>
		private void conciliar(string content)
		{
			lock (thisLock) 
			{
				Conciliador conc = new Conciliador (scanpath);

				conc.addFileContent (content);
				conc.saveArray ();

				conc = null;
			}
		}
	}
}

