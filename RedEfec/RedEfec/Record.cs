﻿using System;

namespace RedEfec
{
	// Record 
	//
	// Record Model for Record File
	//
	// Copyright 2015 Jorge Alberto Ponce Turrubiates
	//
	// Licensed under the Apache License, Version 2.0 (the "License");
	// you may not use this file except in compliance with the License.
	// You may obtain a copy of the License at
	//
	//     http://www.apache.org/licenses/LICENSE-2.0
	//
	// Unless required by applicable law or agreed to in writing, software
	// distributed under the License is distributed on an "AS IS" BASIS,
	// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	// See the License for the specific language governing permissions and
	// limitations under the License.
	public class Record
	{
		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>The identifier.</value>
		public string id { get; set; }

		/// <summary>
		/// Gets or sets the producto.
		/// </summary>
		/// <value>The producto.</value>
		public string producto { get; set; }

		/// <summary>
		/// Gets or sets the importe.
		/// </summary>
		/// <value>The importe.</value>
		public string importe { get; set; }

		/// <summary>
		/// Gets or sets the comision.
		/// </summary>
		/// <value>The comision.</value>
		public string comision { get; set; }

		/// <summary>
		/// Gets or sets the fecha.
		/// </summary>
		/// <value>The fecha.</value>
		public string fecha { get; set; }

		/// <summary>
		/// Gets or sets the hora.
		/// </summary>
		/// <value>The hora.</value>
		public string hora { get; set; }

		/// <summary>
		/// Gets or sets the autorizacion.
		/// </summary>
		/// <value>The autorizacion.</value>
		public string autorizacion { get; set; }

		/// <summary>
		/// Gets or sets the dticks.
		/// </summary>
		/// <value>The dticks.</value>
		public long dticks { get; set; }

		/// <summary>
		/// Gets or sets the oline.
		/// </summary>
		/// <value>The oline.</value>
		public string oline { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="RedEfec.Record"/> class.
		/// </summary>
		/// <param name="line">Line.</param>
		public Record (string line)
		{
			string[] pipes = line.Split ('|');

			id = pipes [0];
			producto = pipes [1];
			importe = pipes [2];
			comision = pipes [3];
			fecha = pipes [4];
			hora = pipes [5];
			autorizacion = pipes [6];
			oline = line;

			int yy = Int32.Parse(fecha.Substring (0, 4));
			int mm = Int32.Parse(fecha.Substring (4, 2));
			int dd = Int32.Parse(fecha.Substring (6, 2));
			int hr = Int32.Parse(hora.Substring (0, 2));
			int mi = Int32.Parse(hora.Substring (2, 2));

			DateTime dt = new DateTime (yy, mm, dd, hr, mi, 0, 0);
			dticks = dt.Ticks;
		}
	}
}

